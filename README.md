# README #


### What is this repository for? ###

* This project allows one to generate cryptographically secure One Time Pads using a simple CSPRNG created by Duthomas.
* Version 0.1

### How do I get set up? ###

* Download Duthomas's library:
https://github.com/Duthomhas/CSPRNG
* Add all the files into your source folder
* Compile

This project was made using Code::Blocks, I suggest using that to compile.

### Usage ###

**./RandomDG.exe OTP.txt 1000** This will generate 1000 characters in OTP.txt using A-Z

**./RandomDG.exe OTP.txt 100 ABC** This will generate 100 characters in OTP.txt using only A B and C.