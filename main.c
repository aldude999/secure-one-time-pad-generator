/**
 *
 * TODO:
 *
 * Common Settings
 * Output to console
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <limits.h>

#include "duthomhas/csprng.h" //CSPRNG Library for Crypto Secure RNG

int main(int argc, char *argv[]) {
    if (argc <= 2 || argc >= 5) //Check Arguments (There should be 2, and 1 of the program name
    {
        printf("Bad number of arguments!\n");
        exit(1);

    }

    CSPRNG rng = csprng_create( rng );        // Check CSPRNG
  if (!rng)
  {
    fprintf( stderr, "%s\n", "No CSPRNG! Fooey!\n" );
    return 1;
  }
    char *p; //Begin data length checking

    long genLength;
    int errno;
    errno = 0;
    genLength = strtol(argv[2], &p, 10); //Add + 1 after 10) for Linux)
    if (errno != 0 || *p != '\0') {
            printf("Bad data length!\n");
        exit(1);
} else {
    // No error
}
    long index, i;
    FILE *randdatafile;
    //time_t t;
    //char characters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *characters = NULL;
    if (argc == 4) {

       characters = argv[3];
       //characters[strlen(argv[3])+1] = '\0';
       //printf(characters);
       //printf(argv[3]);
    }
    else {
        //characters[26] = '\0';
        characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }


    //srand((unsigned) time(&t));

    randdatafile = fopen(argv[1], "w"); //Open data from argument
        if (randdatafile == NULL) {

          printf("Can't open file!");
          exit(1);
    }


                char* result = malloc(genLength); //Add - 1 after genLength for Linux
                result[genLength] = '\0'; //Add - 1 after genLength for Linux

        for (i = 0; i < genLength; i++) { //Add - 1 after genLength for Linux
            index = csprng_get_int( rng ) % strlen(characters);

            result[i] = characters[index];
            csprng_create( rng );
        }


 fprintf(randdatafile, result);



    return 0;

}
